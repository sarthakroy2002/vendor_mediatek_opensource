/*
* Copyright (c) 2004 Michael Niedermayer <michaelni@gmx.at>
* Copyright (c) 2016 Alexandra H��jkov��
* Copyright (c) 2006 Baptiste Coudurier <baptiste.coudurier@free.fr>
* Copyright (c) 2012 Aneesh Dogra (lionaneesh) <lionaneesh@gmail.com>
* Copyright (c) 2021 Mediatek Inc.
*
*
*
* FFmpeg is free software; you can redistribute it and/or
* modify it under the terms of the GNU Lesser General Public
* License as published by the Free Software Foundation; either
* version 2.1 of the License, or (at your option) any later version.
*
* FFmpeg is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
* Lesser General Public License for more details.
*
* You should have received a copy of the GNU Lesser General Public
* License along with FFmpeg; if not, write to the Free Software
* Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA
*/

#ifndef _ALAC_GETBITS_H_
#define _ALAC_GETBITS_H_

#include <string.h>
#include <assert.h>
#include <malloc.h>
#include "alac_utils.h"
#include "alacdec.h"


typedef struct GetByteContext {
    const uint8_t *buffer, *buffer_end, *buffer_start;
} GetByteContext;

#define OPEN_READER_NOSIZE(name, gb)            \
    unsigned int name ## _index = (gb)->index;  \
    unsigned int name ## _cache

#define OPEN_READER(name, gb) OPEN_READER_NOSIZE(name, gb)
#define CLOSE_READER(name, gb) (gb)->index = name ## _index

#define AV_RB32(x)                                \
    (((uint32_t)((const uint8_t*)(x))[0] << 24) |    \
                 (((const uint8_t*)(x))[1] << 16) |    \
                 (((const uint8_t*)(x))[2] <<  8) |    \
                 ((const uint8_t*)(x))[3])

#define NEG_SSR32(a,s) ((( int32_t)(a))>>(32-(s)))
#define NEG_USR32(a,s) (((uint32_t)(a))>>(32-(s)))

#define SHOW_UBITS(name, gb, num) NEG_USR32(name ## _cache, num)
#define SHOW_SBITS(name, gb, num) NEG_SSR32(name ## _cache, num)

#define UPDATE_CACHE(name, gb) name ## _cache = \
      AV_RB32((gb)->buffer + (name ## _index >> 3)) << (name ## _index & 7)

#define BITS_LEFT(name, gb) ((int)((gb)->size_in_bits - name ## _index))

#define LAST_SKIP_BITS(name, gb, num) name ## _index += (num)

static __inline int get_bits_count(const GetBitContext *s)
{
    return s->index;
}

static __inline unsigned int show_bits(GetBitContext *s, int n)
{
    register unsigned int tmp;
    OPEN_READER_NOSIZE(re, s);
    assert(n>0 && n<=25);
    UPDATE_CACHE(re, s);
    tmp = SHOW_UBITS(re, s, n);
    return tmp;
}

static __inline void skip_bits(GetBitContext *s, int n)
{
    OPEN_READER(re, s);
    (void)(re_cache);
    LAST_SKIP_BITS(re, s, n);
    CLOSE_READER(re, s);
}


/**
 * Read 1-25 bits.
 */
static __inline unsigned int get_bits(GetBitContext *s, int n)
{
    register unsigned int tmp;
    OPEN_READER(re, s);
    //assert(n>0 && n<=25);
    UPDATE_CACHE(re, s);
    tmp = SHOW_UBITS(re, s, n);
    LAST_SKIP_BITS(re, s, n);
    CLOSE_READER(re, s);
    //assert(tmp < UINT64_C(1) << n);
    return tmp;
}


static __inline unsigned int get_bits1(GetBitContext *s)
{
    unsigned int index = s->index;
    uint8_t result     = s->buffer[index >> 3];
    result <<= index & 7;
    result >>= 8 - 1;
    index++;
    s->index = index;

    return result;
}

/**
 * Read 0-32 bits.
 */
static __inline unsigned int get_bits_long(GetBitContext *s, int n)
{
    //assert(n>=0 && n<=32);
    if (!n) {
        return 0;
    } else if (n <= MIN_CACHE_BITS) {
        return get_bits(s, n);
    } else {

        unsigned ret = get_bits(s, 16) << (n - 16);
        return ret | get_bits(s, n - 16);
    }
}

static __inline int get_bits_left(GetBitContext *gb)
{
    return gb->size_in_bits - get_bits_count(gb);
}


static __inline int init_get_bits_xe(GetBitContext *s, const uint8_t *buffer,
                                   int bit_size, int is_le)
{
    int buffer_size;
    int ret = 0;

    (void)(is_le);

    if (bit_size >= INT_MAX - FFMAX(7, AV_INPUT_BUFFER_PADDING_SIZE*8) || bit_size < 0 || !buffer) {
        bit_size    = 0;
        buffer      = NULL;
        ret         = -1;
    }

    buffer_size = (bit_size + 7) >> 3;

    s->buffer             = buffer;
    s->size_in_bits       = bit_size;
    s->size_in_bits_plus8 = bit_size + 8;
    s->buffer_end         = buffer + buffer_size;
    s->index              = 0;

    return ret;
}

/**
 * Initialize GetBitContext.
 * @param buffer bitstream buffer, must be AV_INPUT_BUFFER_PADDING_SIZE bytes
 *        larger than the actual read bits because some optimized bitstream
 *        readers read 32 or 64 bit at once and could read over the end
 * @param bit_size the size of the buffer in bits
 * @return 0 on success, AVERROR_INVALIDDATA if the buffer_size would overflow.
 */
static __inline int init_get_bits(GetBitContext *s, const uint8_t *buffer,
                                int bit_size)
{
    return init_get_bits_xe(s, buffer, bit_size, 0);
}

/**
 * Initialize GetBitContext.
 * @param buffer bitstream buffer, must be AV_INPUT_BUFFER_PADDING_SIZE bytes
 *        larger than the actual read bits because some optimized bitstream
 *        readers read 32 or 64 bit at once and could read over the end
 * @param byte_size the size of the buffer in bytes
 * @return 0 on success, AVERROR_INVALIDDATA if the buffer_size would overflow.
 */
static __inline int init_get_bits8(GetBitContext *s, const uint8_t *buffer,
                                 int byte_size)
{
    if (byte_size > INT_MAX / 8 || byte_size < 0)
        byte_size = -1;
    return init_get_bits(s, buffer, byte_size * 8);
}

/**
 * Read 0-32 bits as a signed integer.
 */
static __inline int get_sbits_long(GetBitContext *s, int n)
{
    // sign_extend(x, 0) is undefined
    if (!n)
        return 0;

    return sign_extend(get_bits_long(s, n), n);
}

static __inline int get_sbits(GetBitContext *s, int n)
{
    register int tmp;
    OPEN_READER(re, s);
    assert(n>0 && n<=25);
    UPDATE_CACHE(re, s);
    tmp = SHOW_SBITS(re, s, n);
    LAST_SKIP_BITS(re, s, n);
    CLOSE_READER(re, s);
    return tmp;
}

/**
 * Get unary code of limited length
 * @param gb GetBitContext
 * @param[in] stop The bitstop value (unary code of 1's or 0's)
 * @param[in] len Maximum length
 * @return unary 0 based code index. This is also the length in bits of the
 * code excluding the stop bit.
 * (in case len=1)
 * 1            0
 * 0            1
 * (in case len=2)
 * 1            0
 * 01           1
 * 00           2
 * (in case len=3)
 * 1            0
 * 01           1
 * 001          2
 * 000          3
 */
static __inline int get_unary(GetBitContext *gb, int stop, int len)
{
    int i;

    for(i = 0; i < len && get_bits1(gb) != stop; i++);
    return i;
}

static __inline int get_unary_0_9(GetBitContext *gb)
{
    return get_unary(gb, 0, 9);
}

static __inline void bytestream2_init(GetByteContext *g,
                             const uint8_t *buf,
                             int buf_size)
{
    assert(buf_size >= 0);
    g->buffer       = buf;
    g->buffer_start = buf;
    g->buffer_end   = buf + buf_size;
}

static __inline void bytestream2_skipu(GetByteContext *g,
                              unsigned int size)
{
    g->buffer += size;
}

static __inline uint8_t bytestream2_get_byteu(GetByteContext *g)
{
    if (g->buffer_end - g->buffer < 1) {
        g->buffer = g->buffer_end;
        return 0;
    }
    return *(g->buffer)++;
}

static __inline uint16_t bytestream2_get_be16u(GetByteContext *g)
{
    uint16_t ret;
    const uint8_t* x = g->buffer;
    if (g->buffer_end - g->buffer < 2) {
        g->buffer = g->buffer_end;
        return 0;
    }
    ret = ((((const uint8_t*)(x))[0] << 8) |          \
            ((const uint8_t*)(x))[1]);
    g->buffer += 2;
    return ret;
}

static __inline uint32_t bytestream2_get_be32u(GetByteContext *g)
{
    uint32_t ret;
    const uint8_t* x = g->buffer;
    if (g->buffer_end - g->buffer < 4) {
        g->buffer = g->buffer_end;
        return 0;
    }
    ret = (((uint32_t)((const uint8_t*)(x))[0] << 24) |    \
            (((const uint8_t*)(x))[1] << 16) |    \
            (((const uint8_t*)(x))[2] <<  8) |    \
            ((const uint8_t*)(x))[3]);
    g->buffer += 4;
    return ret;
}


#endif

