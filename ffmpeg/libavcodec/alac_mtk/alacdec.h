/*
* Copyright (c) 2004 Michael Niedermayer <michaelni@gmx.at>
* Copyright (c) 2016 Alexandra H��jkov��
* Copyright (c) 2021 Mediatek Inc.
*
*
*
* FFmpeg is free software; you can redistribute it and/or
* modify it under the terms of the GNU Lesser General Public
* License as published by the Free Software Foundation; either
* version 2.1 of the License, or (at your option) any later version.
*
* FFmpeg is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
* Lesser General Public License for more details.
*
* You should have received a copy of the GNU Lesser General Public
* License along with FFmpeg; if not, write to the Free Software
* Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA
*/


#ifndef _ALAC_H_
#define _ALAC_H_

#ifdef __cplusplus
extern "C" {
#endif

#include <stdint.h>

typedef struct GetBitContext {
    const uint8_t *buffer, *buffer_end;
    int index;
    int size_in_bits;
    int size_in_bits_plus8;
} GetBitContext;

typedef struct alac_file {
    GetBitContext gb;
    int channels;

    int32_t *predict_error_buffer[2];
    int32_t *output_samples_buffer[2];
    int32_t *extra_bits_buffer[2];
    int32_t *bitstream_buffer;
    int32_t  bitstream_bufsize;

    uint32_t max_samples_per_frame;
    uint8_t  sample_size;
    uint8_t  rice_history_mult;
    uint8_t  rice_initial_history;
    uint8_t  rice_limit;
    int      sample_rate;

    int extra_bits;     /**< number of extra bits beyond 16-bit */
    int nb_samples;     /**< number of samples in the current frame */

    int extra_bit_bug;

    int bytespersample;

    uint8_t *extradata;
    int extradata_size;

    uint32_t byteperframe;
} alac_file;

int alac_init(alac_file *alac, uint8_t *inBuf, int insize);

int alac_deinit(alac_file *alac);

void alac_reset(alac_file *alac);

int alac_decode_frame(alac_file *alac,
                      unsigned char *inbuffer,
                      int inputsize,
                      void *outbuffer,
                      int *outputsize);

#ifdef __cplusplus
}
#endif

#endif

